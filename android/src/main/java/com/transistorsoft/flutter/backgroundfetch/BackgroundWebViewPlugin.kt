package com.transistorsoft.flutter.backgroundfetch;

import android.annotation.TargetApi
import android.app.Activity
import android.content.pm.ApplicationInfo
import android.graphics.Bitmap
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import android.webkit.RenderProcessGoneDetail
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import io.flutter.embedding.engine.dart.DartExecutor
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import android.content.Context          
import android.util.Log
import com.transistorsoft.flutter.backgroundfetch.BackgroundFetchModule;


enum class BackgroundCallMethod {
    setOptions, evalJavascript, loadHTML, loadUrl
}

class BackgroundWebViewPlugin(activity: Context): MethodCallHandler {

    companion object {
        lateinit var channel: MethodChannel //todo remove parameter (not used)

        @JvmStatic
        fun registerWith(methodChannel: MethodChannel) {
            channel = methodChannel
        }
    }

    var webView :WebView?=null
    var activityLocal:Context = activity;
    private val webClient = BackgroundWebViewClient(listOf())


    override fun onMethodCall(call: MethodCall, result: Result): Unit {
        val method = BackgroundCallMethod.valueOf(call.method)
        when (method) {
            BackgroundCallMethod.setOptions -> setOptions(call)
            BackgroundCallMethod.evalJavascript -> evalJavascript(call)
            BackgroundCallMethod.loadHTML -> loadHTML(call)
            BackgroundCallMethod.loadUrl -> loadUrl(call)
        }

        result.success(null)
    }

     fun setOptions(call: MethodCall) {
        (call.arguments as? HashMap<*, *>)?.let {
            val restrictedSchemes = it["restrictedSchemes"]
            if (restrictedSchemes is Array<*>)
                webClient.restrictedSchemes = restrictedSchemes.filterIsInstance<String>()
        }
    }

     fun evalJavascript(call: MethodCall) {
        Log.d("#1234567", "evaluating javaScript")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            (call.arguments as? HashMap<*, *>)?.let { arguments ->
                (arguments["script"] as? String)?.let {
                    webView?.evaluateJavascript(it, null)
                }
            }
        }
    }

     fun loadHTML(call: MethodCall) {
            Log.d("#1234567", "#1234567  loading html")
        if(webView==null)
          {  
            webView = WebView(activityLocal)
            val _webView= webView!!
            val params = FrameLayout.LayoutParams(0, 0)
                    // val decorView = activity!!.window.decorView as FrameLayout
                    // decorView.addView(webView, params)

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        if (0!=ApplicationInfo.FLAG_DEBUGGABLE) {
                            WebView.setWebContentsDebuggingEnabled(true)
                        }
                    }

        _webView.visibility = View.GONE
        _webView.settings.javaScriptEnabled = true
        _webView.settings.domStorageEnabled = true
        _webView.settings.allowFileAccessFromFileURLs = true
        _webView.addJavascriptInterface(BackgroundJsInterface(), "native")
        _webView.webViewClient = webClient
        webView = _webView
        }
        (call.arguments as? HashMap<*, *>)?.let { arguments ->
            val html = arguments["html"] as String
            if (arguments.containsKey("baseUrl")) {
                (arguments["baseUrl"] as? String)?.let {
                    webView?.loadDataWithBaseURL(it, html, "text/html", "UTF-8", null)
                }
            } else {
                webView?.loadData(html, "text/html", "UTF-8")
            }
        }
    }

     fun loadUrl(call: MethodCall) {
        (call.arguments as? HashMap<*, *>)?.let { arguments ->
            val url = arguments["url"] as String
            webView?.loadUrl(url)
        }
    }
}

class BackgroundWebViewClient(var restrictedSchemes: List<String>): WebViewClient() {
       override fun onRenderProcessGone(view: WebView, detail: RenderProcessGoneDetail): Boolean {
        if (!detail.didCrash()) {
            // Renderer was killed because the system ran out of memory.
            Log.e("#123467", ("System killed the WebView rendering process to reclaim memory. Recreating..."))

            view?.also { webView ->
                webView.stopLoading()
            }
            // By this point, the instance variable "mWebView" is guaranteed to be null, so it's safe to reinitialize it.

            return true // The app continues executing.
        }

        // Renderer crashed because of an internal error, such as a memory access violation.
        Log.e("#1234567", "The WebView rendering process crashed!")
        return true
    }

    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
        Log.d("#1234567", "#1234567  page started")
        var module = BackgroundFetchModule.getInstance()
        module.invokeMethod(url!!, "didStart", "stateChanged")

        try{
        val data = hashMapOf<String, Any>()
        data["url"] = url!!
        data["type"] = "didStart"
        BackgroundWebViewPlugin.channel.invokeMethod("stateChanged", data)
                }
            catch (e: Exception){
                }


    }

    override fun onPageFinished(view: WebView?, url: String?) {
        Log.d("#1234567", "#1234567 page finished")
        var module = BackgroundFetchModule.getInstance()
        module.invokeMethod(url!!, "didFinish", "stateChanged")
    try{
        val data = hashMapOf<String, Any>()
        data["url"] = url!!
        data["type"] = "didFinish"
        BackgroundWebViewPlugin.channel.invokeMethod("stateChanged", data)
                }
                catch (e: Exception){
                }
        
     }

    override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
        return shouldOverrideUrlLoading(url)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        val url = request?.url.toString()
        return shouldOverrideUrlLoading(url)

    }

    private fun shouldOverrideUrlLoading(url: String?): Boolean {
        for (l in restrictedSchemes) {
            if (url != null && url.contains(l))
                return false
        }

        return true
    }
}

class BackgroundJsInterface {

    @JavascriptInterface
    fun postMessage(data: String?) {
        data?.let {
            val message:HashMap<String?, Any?> = hashMapOf<String?, Any?>()
            message["name"] = "native"

            try {
                when (it[0]) {
                    '{' -> {
                        val jsonObj = JSONObject(it)
                        message["data"] = toMap(jsonObj)
                    }
                    '[' -> {
                        val jsonArray = JSONArray(it)
                        message["data"] = toList(jsonArray)
                    }
                    else -> message["data"] = it
                }
            } catch (e: JSONException) {
                message["data"] = it
            }

            Handler(Looper.getMainLooper()).post {
                var module = BackgroundFetchModule.getInstance()
                module.invokeReturnMethod(message, "didReceiveMessage")
                try{
                BackgroundWebViewPlugin.channel.invokeMethod("didReceiveMessage", message)
                }
                catch (e: Exception){
                }
              
            }
        }
    }

    @Throws(JSONException::class)
    private fun toMap(obj: JSONObject): Map<String, Any> {
        val map = HashMap<String, Any>()

        val keysItr = obj.keys()
        while (keysItr.hasNext()) {
            val key = keysItr.next()
            var value = obj.get(key)

            if (value is JSONArray) {
                value = toList(value)
            } else if (value is JSONObject) {
                value = toMap(value)
            }
            map[key] = value
        }
        return map
    }

    @Throws(JSONException::class)
    private fun toList(array: JSONArray): List<Any> {
        val list = ArrayList<Any>()
        for (i in 0 until array.length()) {
            var value = array.get(i)
            if (value is JSONArray) {
                value = toList(value)
            } else if (value is JSONObject) {
                value = toMap(value)
            }
            list.add(value)
        }
        return list
    }
}



