package com.transistorsoft.flutter.backgroundfetch;

import androidx.annotation.NonNull;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import android.util.Log;

/** BackgroundFetchPlugin */
public class BackgroundFetchPlugin implements FlutterPlugin, ActivityAware {
    public static final String TAG                          = "TSBackgroundFetch";

    // @deprecated Not used by v2.
    public static void registerWith(Registrar registrar) {
        Log.d("BackgroundFetch.TAG", "#1234567---engine registerWith----");

        BackgroundFetchModule module = BackgroundFetchModule.getInstance();
        module.onAttachedToEngine(registrar.context(), registrar.messenger());
        if (registrar.activity() != null) {
            module.setActivity(registrar.activity());
        }
    }

    // @deprecated Called by Application#onCreate
    public static void setPluginRegistrant(PluginRegistry.PluginRegistrantCallback callback) {
        Log.d("BackgroundFetch.TAG", "#1234567---engine setPluginRegistrant----");

        HeadlessTask.setPluginRegistrant(callback);
    }

    public BackgroundFetchPlugin() { }

    @Override
    public void onAttachedToEngine(FlutterPlugin.FlutterPluginBinding binding) {
        Log.d("BackgroundFetch.TAG", "#1234567---engine attached----");
        BackgroundFetchModule.getInstance().onAttachedToEngine(binding.getApplicationContext(), binding.getBinaryMessenger());
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        Log.d("BackgroundFetch.TAG", "#1234567---engine detached----");

        BackgroundFetchModule.getInstance().onDetachedFromEngine();
    }

    @Override
    public void onAttachedToActivity(ActivityPluginBinding activityPluginBinding) {
        Log.d("BackgroundFetch.TAG", "#1234567---activity attached----");

        BackgroundFetchModule.getInstance().setActivity(activityPluginBinding.getActivity());
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {
        Log.d("BackgroundFetch.TAG", "#1234567---activity detached for config changes----");

        // TODO: the Activity your plugin was attached to was
        // destroyed to change configuration.
        // This call will be followed by onReattachedToActivityForConfigChanges().
    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding activityPluginBinding) {
        // TODO: your plugin is now attached to a new Activity
        // after a configuration change.
        Log.d("BackgroundFetch.TAG", "#1234567---BackgroundFetchPlugin: 66 onReattachedToActivityForConfigChanges----");

    }

    @Override
    public void onDetachedFromActivity() {
        Log.d("BackgroundFetch.TAG", "#1234567---activity detached----");

        BackgroundFetchModule.getInstance().setActivity(null);
    }
}
